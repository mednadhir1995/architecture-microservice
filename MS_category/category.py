from flask import Flask, request, json, Response
from pymongo import MongoClient
import logging as log
from bson.objectid import ObjectId
import pika
from flask_cors import CORS

app = Flask(__name__)
CORS(app)


class Encoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, ObjectId):
            return str(obj)
        else:
            return obj
class MongoAPI:
    def __init__(self, data):
        log.basicConfig(level=log.DEBUG, format='%(asctime)s %(levelname)s:\n%(message)s\n')
        #self.client = MongoClient("mongodb://35.180.206.6:32803/")  # When only Mongo DB is running on Docker.
        self.client = MongoClient("mongodb://localhost:27017/")      # When both Mongo and This application is running on
                                                                    # Docker and we are using Docker Compose

        database = 'delivs-market-orderdb'
        collection = 'orders'
        cursor = self.client[database]
        self.collection = cursor[collection]
        self.data = data

    def read(self):
        log.info('Reading All Data')
        filt = self.data
        documents = self.collection.find(filt)
        output = [{item: data[item] for item in data } for data in documents]
        return output
    def read_one(self, data):
        log.info('Reading one Data')
        documents = self.collection.find_one({'_id': ObjectId(data)})
        #output = [{item: data[item] for item in data } for data in documents]
        return documents

    def write(self, data):
        log.info('Writing Data')
        new_document = data
        response = self.collection.insert_one(new_document)
        output = {'Status': 'Successfully Inserted',
                  'Document_ID': str(response.inserted_id)}
        return output

    def update_one(self):
        log.info('Updating Data')
        filt = {'_id': ObjectId(self.data['_id'])}
        updated_data = {"$set": self.data['data']}
        response = self.collection.update_one(filt, updated_data)
        output = {'Status': 'Successfully Updated' if response.modified_count > 0 else "Nothing was updated."}
        return output

    def update(self):
        log.info('Updating Data')
        filt = self.data['filter']
        updated_data = {"$set": self.data['data']}
        response = self.collection.update_one(filt, updated_data)
        output = {'Status': 'Successfully Updated' if response.modified_count > 0 else "Nothing was updated."}
        return output

    def delete(self, data):
        log.info('Deleting Data')
        filt = {'_id': ObjectId(self.data['_id'])}
        response = self.collection.delete_one(filt)
        output = {'Status': 'Successfully Deleted' if response.deleted_count > 0 else "Document not found."}
        return output


@app.route('/')
def base():
    return Response(response=json.dumps({"Status": "UP"},cls=Encoder),
                    status=200,
                    mimetype='application/json')


@app.route('/category', methods=['GET'])
def mongo_read():
    data = request.args
    obj1 = MongoAPI(data)
    response = obj1.read()
    return Response(response=json.dumps(response, cls=Encoder),
                    status=200,
                    mimetype='application/json')
@app.route('/categoryById', methods=['GET'])
def mongo_read_one():
    data = request.args.get("id")

    obj1 = MongoAPI(data)
    response = obj1.read_one(data)
    return Response(response=json.dumps(response, cls=Encoder),
                    status=200,
                    mimetype='application/json')

@app.route('/category', methods=['POST'])
def mongo_write():
    data = request.json
    if data is None or data == {}:
        return Response(response=json.dumps({"Error": "Please provide connection information"}),
                        status=400,
                        mimetype='application/json')
    obj1 = MongoAPI(data)
    response = obj1.write(data)
    return Response(response=json.dumps(response, cls=Encoder),
                    status=200,
                    mimetype='application/json')

@app.route('/category', methods=['PUT'])
def mongo_update_one():
    data = request.json
    if data is None or data == {}:
        return Response(response=json.dumps({"Error": "Please provide connection information"}),
                        status=400,
                        mimetype='application/json')
    obj1 = MongoAPI(data)
    response = obj1.update_one()
    return Response(response=json.dumps(response, cls=Encoder),
                    status=200,
                    mimetype='application/json')
@app.route('/categoryByFilter', methods=['PUT'])
def mongo_update():
    data = request.json
    if data is None or data == {} or 'filter' not in data:
        return Response(response=json.dumps({"Error": "Please provide connection information"}),
                        status=400,
                        mimetype='application/json')
    obj1 = MongoAPI(data)
    response = obj1.update()
    return Response(response=json.dumps(response, cls=Encoder),
                    status=200,
                    mimetype='application/json')


@app.route('/category', methods=['DELETE'])
def mongo_delete():
    data = request.json
    if data is None or data == {} :
        return Response(response=json.dumps({"Error": "Please provide connection information"}),
                        status=400,
                        mimetype='application/json')
    obj1 = MongoAPI(data)
    response = obj1.delete(data)
    return Response(response=json.dumps(response, cls=Encoder),
                    status=200,
                    mimetype='application/json')


if __name__ == '__main__':
    app.run(debug=True, port=5003, host='0.0.0.0')


