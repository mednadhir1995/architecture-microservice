package com.example.demo.Model;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.lang.Double;
import com.example.demo.Model.Commande;
import java.util.List;
@RepositoryRestResource
@CrossOrigin(origins = "*")
public interface CommandeRepository extends MongoRepository<Commande, String>
{
	@RestResource(path="/byprice")
	@CrossOrigin(origins = "*")
	List<Commande> findByMontantGreaterThan(@Param("mt")Double montant);

	 @RestResource(path="/bypriceparP")
	 @CrossOrigin(origins = "*")
	 Page<Commande> findByMontantGreaterThan(@Param("mt")Double montant, Pageable P);
}
